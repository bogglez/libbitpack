all: example

example: bitpack.o example.o
	$(CC) $^ -o $@

%.o: %.c %.h
	$(CC) -c $(filter %.c,$^)

%.o: %.c
	$(CC) -c $^

clean:
	rm *.o example
