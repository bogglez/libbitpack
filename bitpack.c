#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include "bitpack.h"

bitpack_Serializer bitpack_writer;
bitpack_Serializer bitpack_reader;

void bitpack_skipBitsInBitBuffer(bitpack_BitBuffer *b, size_t bits) {
	assert(b && b->currentBit + bits < b->bitCount);
	b->currentBit += bits;
}

// Helper functions for writing
static void bitpack_writeBitToBitBuffer(bitpack_BitBuffer *b, uint8_t const value) {
	assert(b && b->currentBit < b->bitCount && (value == 1 || !value));
	b->buffer[b->currentBit / 8] |= (uint8_t)(value << BITPACK_SHIFT_BIT(b->currentBit));
	bitpack_skipBitsInBitBuffer(b, 1);
}

// Helper functions for reading
static unsigned bitpack_readBitFromBitBuffer(bitpack_BitBuffer *b) {
	assert(b && b->currentBit < b->bitCount);
	unsigned value = (b->buffer[b->currentBit / 8] >> BITPACK_SHIFT_BIT(b->currentBit)) & 1;
	bitpack_skipBitsInBitBuffer(b, 1);
	return value;
}


// Serialization functions for writing and reading
// Write
static void bitpack_writeBoolToBitBuffer(bitpack_BitBuffer * restrict b, uint8_t * restrict const value) {
	assert(b && b->currentBit < b->bitCount && value);
	bitpack_writeBitToBitBuffer(b, !!*value);
}

static void bitpack_writeU8ToBitBuffer(bitpack_BitBuffer * restrict b, uint8_t * restrict const value) {
	assert(b && b->currentBit + 8 <= b->bitCount && value);

	size_t  currentByte      = b->currentBit / 8;
	size_t  currentBitInByte = b->currentBit % 8;
	uint8_t *buffer          = b->buffer;

	if(currentBitInByte) {
		buffer[currentByte  ] |= (uint8_t)(*value >> currentBitInByte);
		buffer[++currentByte] |= (uint8_t)(*value << (8 - currentBitInByte));
	}
	else {
		buffer[currentByte] = *value;
	}

	bitpack_skipBitsInBitBuffer(b, 8);
}

static void bitpack_writeU16ToBitBuffer(bitpack_BitBuffer * restrict b, uint16_t * restrict const value) {
	assert(b && b->currentBit + 16 <= b->bitCount && value);
	size_t  currentByte      = b->currentBit / 8;
	size_t  currentBitInByte = b->currentBit % 8;
	uint8_t *buffer          = b->buffer;

	if(currentBitInByte) {
		buffer[currentByte  ] |= (uint8_t)(*value >> (currentBitInByte + 8));
		buffer[++currentByte] |= (uint8_t)(*value >> currentBitInByte);
		buffer[++currentByte] |= (uint8_t)(*value << (8 - currentBitInByte));
	}
	else {
		buffer[currentByte]   = (uint8_t)(*value >> 8);
		buffer[++currentByte] = (uint8_t)(*value & 0xff);
	}

	bitpack_skipBitsInBitBuffer(b, 16);
}

static void bitpack_writeU32ToBitBuffer(bitpack_BitBuffer * restrict b, uint32_t * restrict const value) {
	assert(b && b->currentBit + 32 <= b->bitCount && value);
	size_t  currentByte      = b->currentBit / 8;
	size_t  currentBitInByte = b->currentBit % 8;
	uint8_t *buffer          = b->buffer;

	if(currentBitInByte)
	{
		buffer[currentByte  ] |= (uint8_t)(*value >> (currentBitInByte + 24));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 16));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte +  8));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte));
		buffer[++currentByte] |= (uint8_t)(*value << (8 - currentBitInByte));
	}
	else {
		buffer[currentByte]   = (uint8_t)(*value >> 24);
		buffer[++currentByte] = (uint8_t)(*value >> 16);
		buffer[++currentByte] = (uint8_t)(*value >>  8);
		buffer[++currentByte] = (uint8_t)(*value      );
	}

	bitpack_skipBitsInBitBuffer(b, 32);
}

static void bitpack_writeU64ToBitBuffer(bitpack_BitBuffer * restrict b, uint64_t * restrict const value) {
	assert(b && b->currentBit + 64 <= b->bitCount && value);
	size_t  currentByte      = b->currentBit / 8;
	size_t  currentBitInByte = b->currentBit % 8;
	uint8_t *buffer          = b->buffer;

	if(currentBitInByte) {
		buffer[currentByte  ] |= (uint8_t)(*value >> (currentBitInByte + 56));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 48));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 40));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 32));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 24));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte + 16));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte +  8));
		buffer[++currentByte] |= (uint8_t)(*value >> (currentBitInByte));
		buffer[++currentByte] |= (uint8_t)(*value << (8 - currentBitInByte));
	}
	else {
		buffer[currentByte]   = (uint8_t)(*value >> 56);
		buffer[++currentByte] = (uint8_t)(*value >> 48);
		buffer[++currentByte] = (uint8_t)(*value >> 40);
		buffer[++currentByte] = (uint8_t)(*value >> 32);
		buffer[++currentByte] = (uint8_t)(*value >> 24);
		buffer[++currentByte] = (uint8_t)(*value >> 16);
		buffer[++currentByte] = (uint8_t)(*value >>  8);
		buffer[++currentByte] = (uint8_t)(*value);
	}

	bitpack_skipBitsInBitBuffer(b, 64);
}

static void bitpack_writeFloatToBitBuffer(bitpack_BitBuffer * restrict b, float * restrict const value) {
	assert(b && b->currentBit + 32 <= b->bitCount && value);
	bitpack_writeU32ToBitBuffer(b, (uint32_t * const) value);
}

static void bitpack_writeDoubleToBitBuffer(bitpack_BitBuffer *b, double * const value) {
	assert(b && b->currentBit + 64 <= b->bitCount && value);
	bitpack_writeU64ToBitBuffer(b, (uint64_t * const) value);
}

static void bitpack_writeU32RangeToBitBuffer(bitpack_BitBuffer * restrict b, uint32_t * restrict const value, uint32_t const minValue, uint32_t const maxValue) {
	assert(b && value && minValue <= *value && *value <= maxValue);

	uint32_t relativeMax   = maxValue - minValue;
	uint32_t relativeValue = *value   - minValue;
	size_t bitsInInterval;

	for(bitsInInterval = 1; (1 << bitsInInterval) < relativeMax; ++bitsInInterval);
	for(size_t i = 0; i < bitsInInterval; ++i) {
		bitpack_writeBitToBitBuffer(b, (relativeValue >> (bitsInInterval-i-1)) & 1);
	}
}

// Read
static void bitpack_readBoolFromBitBuffer(bitpack_BitBuffer * restrict b, uint8_t * restrict const value) {
	assert(b && b->currentBit < b->bitCount && value);
	*value = !!(b->buffer[b->currentBit / 8] & BITPACK_MASK_BIT(b->currentBit));
	bitpack_skipBitsInBitBuffer(b, 1);
}

static void bitpack_readU8FromBitBuffer(bitpack_BitBuffer * restrict b, uint8_t * restrict const value) {
	assert(b && b->currentBit + 8 <= b->bitCount);
	size_t  currentByte          = b->currentBit / 8;
	size_t  currentBitInByte     = b->currentBit % 8;
	uint8_t const * const buffer = b->buffer;

	if(currentBitInByte) {
		*value = (uint8_t)((buffer[currentByte  ] << currentBitInByte)
		       | (buffer[currentByte+1] >> (8 - currentBitInByte)));
	}
	else {
		*value = (uint8_t)(buffer[currentByte]);
	}

	bitpack_skipBitsInBitBuffer(b, 8);
}

static void bitpack_readU16FromBitBuffer(bitpack_BitBuffer * restrict b, uint16_t * restrict const value) {
	assert(b && b->currentBit + 16 <= b->bitCount);
	size_t currentByte           = b->currentBit / 8;
	size_t currentBitInByte      = b->currentBit % 8;
	uint8_t const * const buffer = b->buffer;

	if(currentBitInByte) {
		*value = (uint16_t) ((buffer[currentByte  ] << (currentBitInByte+8))
		       | (buffer[currentByte+1] << currentBitInByte)
		       | (buffer[currentByte+2] >> (8-currentBitInByte)));
	}
	else {
		*value = (uint16_t)((buffer[currentByte] << 8) | buffer[currentByte+1]);
	}

	bitpack_skipBitsInBitBuffer(b, 16);
}

static void bitpack_readU32FromBitBuffer(bitpack_BitBuffer * restrict b, uint32_t * restrict const value)
{
	assert(b && b->currentBit + 32 <= b->bitCount);
	size_t currentByte           = b->currentBit / 8;
	size_t currentBitInByte      = b->currentBit % 8;
	uint8_t const * const buffer = b->buffer;

	if(currentBitInByte) {
		*value = (uint32_t)((buffer[currentByte  ] << (currentBitInByte+24))
		      | (buffer[currentByte+1] << (currentBitInByte+16))
		      | (buffer[currentByte+2] << (currentBitInByte+ 8))
		      | (buffer[currentByte+3] << currentBitInByte)
		      | (buffer[currentByte+4] >> (8-currentBitInByte)));
	}
	else {
		*value = (uint32_t)((buffer[currentByte  ] << 24)
		       | (buffer[currentByte+1] << 16)
		       | (buffer[currentByte+2] <<  8)
		       |  buffer[currentByte+3]);
	}

	bitpack_skipBitsInBitBuffer(b, 32);
}

static void bitpack_readU64FromBitBuffer(bitpack_BitBuffer * restrict b, uint64_t * restrict const value)
{
	assert(b && b->currentBit + 64 <= b->bitCount);
	size_t currentByte           = b->currentBit / 8;
	size_t currentBitInByte      = b->currentBit % 8;
	uint8_t const * const buffer = b->buffer;

	if(currentBitInByte) {
		*value = (((uint64_t)buffer[currentByte+0]) << currentBitInByte << 56)
		       | (((uint64_t)buffer[currentByte+1]) << currentBitInByte << 48)
		       | (((uint64_t)buffer[currentByte+2]) << currentBitInByte << 40)
		       | (((uint64_t)buffer[currentByte+3]) << currentBitInByte << 32)
		       | (((uint64_t)buffer[currentByte+4]) << currentBitInByte << 24)
		       | (((uint64_t)buffer[currentByte+5]) << currentBitInByte << 16)
		       | (((uint64_t)buffer[currentByte+6]) << currentBitInByte << 8)
		       | (((uint64_t)buffer[currentByte+7]) << currentBitInByte << 0)
		       | (((uint64_t)buffer[currentByte+8]) >> (8-currentBitInByte));
	}
	else {
		*value = (((uint64_t)buffer[currentByte  ]) << 56)
		       | (((uint64_t)buffer[currentByte+1]) << 48)
		       | (((uint64_t)buffer[currentByte+2]) << 40)
		       | (((uint64_t)buffer[currentByte+3]) << 32)
		       | (((uint64_t)buffer[currentByte+4]) << 24)
		       | (((uint64_t)buffer[currentByte+5]) << 16)
		       | (((uint64_t)buffer[currentByte+6]) <<  8)
		       | ((uint64_t)(buffer[currentByte+7]));
	}

	bitpack_skipBitsInBitBuffer(b, 64);
}

static void bitpack_readFloatFromBitBuffer(bitpack_BitBuffer * restrict b, float * restrict const value) {
	assert(b && value);
	bitpack_readU32FromBitBuffer(b, (uint32_t * const) value);
}

static void bitpack_readDoubleFromBitBuffer(bitpack_BitBuffer * restrict b, double * restrict const value) {
	assert(b && value);
	bitpack_readU64FromBitBuffer(b, (uint64_t * const) value);
}

static void bitpack_readU32RangeFromBitBuffer(bitpack_BitBuffer * restrict b, uint32_t * restrict const value, uint32_t const minValue, uint32_t const maxValue) {
	assert(b && value && minValue <= *value && *value <= maxValue);

	uint32_t const relativeMax = maxValue - minValue;

	size_t bitsInInterval;
	for(bitsInInterval = 1; (1 << bitsInInterval) < relativeMax; ++bitsInInterval);

	uint32_t relativeValue = 0;
	unsigned bit;
	for(size_t i = 0; i < bitsInInterval; ++i)
	{
		bit = bitpack_readBitFromBitBuffer(b);	
		relativeValue |= bit << (bitsInInterval-i-1);
	}

	*value = relativeValue + minValue;
}


static void initCallbacks() {
	bitpack_reader.OnBool     = bitpack_readBoolFromBitBuffer;
	bitpack_reader.OnU8       = bitpack_readU8FromBitBuffer;
	bitpack_reader.OnU16      = bitpack_readU16FromBitBuffer;
	bitpack_reader.OnU32      = bitpack_readU32FromBitBuffer;
	bitpack_reader.OnU64      = bitpack_readU64FromBitBuffer;
	bitpack_reader.OnFloat    = bitpack_readFloatFromBitBuffer;
	bitpack_reader.OnDouble   = bitpack_readDoubleFromBitBuffer;
	bitpack_reader.OnU32Range = bitpack_readU32RangeFromBitBuffer;

	bitpack_writer.OnBool     = bitpack_writeBoolToBitBuffer;
	bitpack_writer.OnU8       = bitpack_writeU8ToBitBuffer;
	bitpack_writer.OnU16      = bitpack_writeU16ToBitBuffer;
	bitpack_writer.OnU32      = bitpack_writeU32ToBitBuffer;
	bitpack_writer.OnU64      = bitpack_writeU64ToBitBuffer;
	bitpack_writer.OnFloat    = bitpack_writeFloatToBitBuffer;
	bitpack_writer.OnDouble   = bitpack_writeDoubleToBitBuffer;
	bitpack_writer.OnU32Range = bitpack_writeU32RangeToBitBuffer;
}

void bitpack_initBitBuffer(bitpack_BitBuffer * restrict const b, size_t const bytes, uint8_t * restrict const buffer) {
	initCallbacks(); // Init reader and writer callbacks here so user doesn't need to do it manually

	b->bitCount   = 8 * bytes;
	b->currentBit = 0;
	b->buffer     = buffer;
}

void bitpack_rewindBitBuffer(bitpack_BitBuffer * const b) {
	b->currentBit = 0;
}
