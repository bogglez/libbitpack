#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "bitpack.h"

// Define network packets that should be bit-packed for sending or received in a bit-packed form
typedef struct {
	uint64_t uuid;
	float    x, y, z;
	float    rotation;
	uint32_t maxHealth;
	uint32_t health;
	uint8_t  isVisible;
	uint8_t  isMale;
	uint8_t  isAdmin;
} PlayerInfoPacket;


// Show what the representation will be after packing
static void printBuffer(uint8_t const * const buffer, size_t const bufferSize) {
	for(size_t i = 0; i < 8 * bufferSize; ++i) {
		if(i && !(i % 8)) {
			putchar(' ');
		}
		putchar('0' + ((buffer[i / 8] & BITPACK_MASK_BIT(i)) >> BITPACK_SHIFT_BIT(i)));
	}
	puts("");
}


// Instruct a visitor (serializer or deserializer) to visit each element of the packet.
static void visitPlayerInfoPacket(PlayerInfoPacket *p, bitpack_BitBuffer *buffer, bitpack_Serializer *visitor) {
	visitor->OnU64  (buffer, &p->uuid); // a serializer   will write the member variable of `p' in `buffer' in a bit-packed form
	visitor->OnFloat(buffer, &p->x);    // a deserializer will read  the member variable of `p' from `buffer', decoding the bit-packed form
	visitor->OnFloat(buffer, &p->y);
	visitor->OnFloat(buffer, &p->z);
	visitor->OnU32  (buffer, &p->maxHealth);

	// Since health is in [0, maxHealth] and maxHealth is known, only ld(maxHealth) bits are needed
	visitor->OnU32Range(buffer, &p->health, 0, p->maxHealth);

	visitor->OnU8   (buffer, &p->isVisible);
	visitor->OnU8   (buffer, &p->isMale);
	visitor->OnU8   (buffer, &p->isAdmin);
}

int main() {
	// Specify a buffer where bit-packed packets should be stored, ready for sending over the network
	size_t const bufferSize     = 32; // 32 bytes = 32 * 8 bits
	uint8_t      * const buffer = malloc(bufferSize);

	// BitBuffer will keep track of the current position in the buffer
	bitpack_BitBuffer bitBuffer;
	bitpack_initBitBuffer(&bitBuffer, bufferSize, buffer);

	// User data to pack
	PlayerInfoPacket packet;
	packet.uuid      = 0xDEADBABE;
	packet.x         = 10.123f;
	packet.y         = 20.456f;
	packet.z         = 30.789f;
	packet.maxHealth = 500;
	packet.health    = 400;
	packet.isVisible = 1;
	packet.isMale    = 1;
	packet.isAdmin   = 0;

	// Tell a serializer to pack `packet' into `buffer' in a bit-packed form
	visitPlayerInfoPacket(&packet, &bitBuffer, &bitpack_writer);	

	// Print the bit-packed data for demonstration purposes
	printBuffer(buffer, bufferSize);

	// Reset the position in the bit buffer and forget the contents of the packet in order
	// to demonstrate how `packet' can be extracted from received, bit-packed data in `buffer'
	bitpack_rewindBitBuffer(&bitBuffer);
	memset(&packet, 0, sizeof(packet));

	// Tell a deserializer to unpack `buffer' into `packet'.
	visitPlayerInfoPacket(&packet, &bitBuffer, &bitpack_reader);

	// Print out received packet in unpacked form
	printf("Player Info Packet:\n UUID: %08lX\n Pos: %g %g %g\n Health: %u/%u\n Visible: %s\n Gender: %s\n Admin: %s\n",
		packet.uuid, packet.x, packet.y, packet.z, packet.health, packet.maxHealth, packet.isVisible ? "yes" : "no", packet.isMale ? "male" : "female", packet.isAdmin ? "yes" : "no");

	if(buffer) {
		free(buffer);
	}
}
