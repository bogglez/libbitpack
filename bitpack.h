#ifndef BITPACK_H
#define BITPACK_H

#include <stddef.h>
#include <stdint.h>

// SHIFT_BIT(0): 7  (position at first bit of byte  1000 0000)
// SHIFT_BIT(1): 6  (position at second bit of byte 0100 0000)
#define BITPACK_SHIFT_BIT(N) (7 - ((N) % 8))

// MASK_BIT(0): 1000 0000
// MASK_BIT(1): 0100 0000
#define BITPACK_MASK_BIT(N) (1 << BITPACK_SHIFT_BIT(N))

typedef struct {
	size_t bitCount;
	size_t currentBit;
	uint8_t *buffer;
} bitpack_BitBuffer;

typedef struct {
	void (*OnBool    )(bitpack_BitBuffer * restrict b, uint8_t  * restrict const value);
	void (*OnU8      )(bitpack_BitBuffer * restrict b, uint8_t  * restrict const value);
	void (*OnU16     )(bitpack_BitBuffer * restrict b, uint16_t * restrict const value);
	void (*OnU32     )(bitpack_BitBuffer * restrict b, uint32_t * restrict const value);
	void (*OnU64     )(bitpack_BitBuffer * restrict b, uint64_t * restrict const value);
	void (*OnFloat   )(bitpack_BitBuffer * restrict b, float    * restrict const value);
	void (*OnDouble  )(bitpack_BitBuffer * restrict b, double   * restrict const value);
	void (*OnU32Range)(bitpack_BitBuffer * restrict b, uint32_t * restrict const value, uint32_t const minValue, uint32_t const maxValue);
} bitpack_Serializer;

void bitpack_initBitBuffer       (bitpack_BitBuffer * restrict const, size_t const byteCount, uint8_t * restrict const memory);
void bitpack_rewindBitBuffer     (bitpack_BitBuffer * const);
void bitpack_skipBitsInBitBuffer (bitpack_BitBuffer * const,          size_t const bitCount);

extern bitpack_Serializer bitpack_reader;
extern bitpack_Serializer bitpack_writer;

#endif
